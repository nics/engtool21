#include "SPHSim.h"

/////////////////////////////////////
/////////////// EX 4 ////////////////
/////////////////////////////////////
void SPHSim::computeDensity() {
	int n = m_pParticleData->getNumParticles();
	auto position = m_pParticleData->getPositions();
	vector<float>& density = OwnCustomAttribute<vector<float>>::get(m_pParticleData)->getAttribute("density");

	// there is no neighbor search, O(n^2)
	#pragma omp parallel for
	for (int i = 0; i < n; ++i) {
		density[i] = 0;
		//for (int j = 0; j < n; ++j) {
		for (int j : getNeighbors(position[i])) {
			/////
			// TODO: compute density
			// hints: m_mass, W()
			// ...
		}
	}
}

void SPHSim::computePressure() {
	int n = m_pParticleData->getNumParticles();
	auto density = OwnCustomAttribute<vector<float>>::get(m_pParticleData)->getAttribute("density");
	vector<float>& pressure = OwnCustomAttribute<vector<float>>::get(m_pParticleData)->getAttribute("pressure");
		
#pragma omp parallel for
	for (int i = 0; i < n; ++i) {
		/////
		// TODO: compute pressure
		// hints: m_k (stiffness), m_exp (exponent), m_rest (rest density)
		// ...

		// avoid attraction from negative pressure
		pressure[i] = std::max((double)pressure[i], 0.0);
	}
}

void SPHSim::computeForce() {
	int n = m_pParticleData->getNumParticles();
	auto density = OwnCustomAttribute<vector<float>>::get(m_pParticleData)->getAttribute("density");
	auto pressure = OwnCustomAttribute<vector<float>>::get(m_pParticleData)->getAttribute("pressure");
	auto position = m_pParticleData->getPositions();
	auto velocity = m_pParticleData->getVelocities();
	vector<Eigen::Vector3d>& force = OwnCustomAttribute<vector<Eigen::Vector3d>>::get(m_pParticleData)->getAttribute("force");

	// there is no neighbor search, O(n^2)
	#pragma omp parallel for
	for (int i = 0; i < n; ++i) {
		Eigen::Vector3d pressure_force, viscosity_force;
		pressure_force.setZero();
		viscosity_force.setZero();

		/////
		// TODO: compute pressure and viscosity force
		// hints: gradW(), m_mu (viscosity), lapW()
		//for (int j = 0; j < n; ++j) {
		for (int j : getNeighbors(position[i])) {
			// ...
		}

		force[i] = pressure_force + viscosity_force + m_gravity;
	}
}